Short and sweet Javascript module to generate fractals and draw to a HTML5 canvas.

TODO:
- only half-hexagon is supported, need to allow half- and full- polygons of an arbitrary number of sides
- skew angles cause the whole fractal to rotate and move off-centre. 
- increasing depth causes the fractal to grow to the right (off-screen). 