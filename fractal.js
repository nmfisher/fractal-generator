/********************************************************************************/
/** Copyright 2017 Avinium Pty Ltd                                              */
/**                                                                             */
/**   Licensed under the Apache License, Version 2.0 (the "License");           */
/**   you may not use this file except in compliance with the License.          */    
/**   You may obtain a copy of the License at                                   */
/**                                                                             */
/**   http://www.apache.org/licenses/LICENSE-2.0                                */        
/**                                                                             */
/**   Unless required by applicable law or agreed to in writing, software       */
/**   distributed under the License is distributed on an "AS IS" BASIS,         */
/**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  */
/**   See the License for the specific language governing permissions and       */
/**   limitations under the License.                                            */
/********************************************************************************/

// cache cos/sin values - does Math do this natively?
var cos_lookup = {}
var sin_lookup = {}

var cos = function(angle) {
    if(typeof(cos_lookup[angle]) === "undefined") {
        cos_lookup[angle] = Math.cos(angle)
    }
    return cos_lookup[angle]
}

var sin = function(angle) {
    if(typeof(sin_lookup[angle]) === "undefined") {
        sin_lookup[angle] = Math.sin(angle)
    }
    return sin_lookup[angle]
}

var Point = function(x, y) {
    if(isNaN(x) || isNaN(y)) {
        throw "NaN error"
    }
    this.x = x;
    this.y = y;
}
   
Point.prototype.add = function(delta) {
    return new Point(this.x + delta.x, this.y + delta.y);
}

Point.prototype.minus = function(delta) {
    return new Point(this.x - delta.x, this.y - delta.y);
}

Point.prototype.reflect = function(line) {
    var slope = line.slope();
    if(slope == 0) {
        return new Point(this.x, (2 * line.points[0].y - this.y))
    } else if(typeof(slope) == "undefined") {
        return new Point((2 * line.points[0].x - this.x), this.y)
    }
    var slope_ = -1/slope;
    var intercept_ = this.y - (slope_ * this.x);
    var x_ = (intercept_ - line.intercept()) / (slope - slope_);
    var y_ = (slope_ * x_) + intercept_
    var y_sign = (y_ > this.y) ? 1 : -1;
    var x_sign = (x_ > this.x) ? 1 : -1;
    var reflected = new Point(this.x + x_sign * (2 * Math.abs(x_ - this.x)), this.y + y_sign * ( 2 * Math.abs(y_ - this.y)));
    return reflected
}

Point.prototype.translate = function(x_step, y_step) {
    if(isNaN(x_step) || isNaN(y_step)) {
        throw "NaN error"
    }
    return new Point(this.x + x_step, this.y + y_step)
}
    
Point.prototype.rotate = function(around, angle) {
    if(angle == 0) {
        console.log("unchanged")
        return new Point(this.x, this.y)
    }
    // translate so we are rotating around the origin
    var this_ = this.translate(-around.x, -around.y);
    // rotate
    var x_ = (this_.x * cos(angle)) - (this_.y * sin(angle));
    var y_ = (this_.x * sin(angle)) + (this_.y * cos(angle));
    // translate back
    var translated = new Point(x_, y_).translate(around.x, around.y);
    return translated
}

Point.prototype.skew = function(angle) {
    return new Point(this.x + (angle * this.y),this.y)
}

Point.prototype.y_skew = function(angle) {
    return new Point(this.x, (angle * this.x)+this.y)
}

Point.prototype.scale = function(factor) {
    var x_factor = this.x * factor;
    var y_factor = this.y * factor;
    return new Point(x_factor, y_factor)
}

Point.prototype.copy = function() {
    return new Point(this.x, this.y)
}

var Line = function(point1, point2) {
    this.points = [point1, point2]
    this._intercept;
    this._angle;
    this._slope;

}

Line.prototype.delta_y = function() { return this.points[1].y - this.points[0].y };
Line.prototype.delta_x = function() { return this.points[1].x - this.points[0].x };
Line.prototype.slope = function() { 
    if(this.delta_y() == 0) {
        return 0;
    } else if (this.delta_x() !=0) {
        this._slope = this.delta_y() / this.delta_x()        
    } 
    return this._slope
}

Line.prototype.intercept = function() { 
    if(typeof(this._intercept) === "undefined") {
        this._intercept = this.points[0].y - (this.slope() * this.points[0].x)
    }
    return this._intercept
}
Line.prototype.length = function() { 
    return Math.sqrt(Math.pow(this.delta_y(),2) + Math.pow(this.delta_x(),2));   
}

Line.prototype.angle = function() { 
    var delta_y = this.delta_y()
    var delta_x = this.delta_x()
    if(delta_y !=    0) {
        this._angle = Math.acos(delta_x / this.length());        
    } else {
        this._angle = 0;        
    }
    return this._angle
}
Line.prototype.draw = function(ctx) {
    ctx.beginPath();    
    var p0 = this.points[0];
    var p1 = this.points[1];
    ctx.moveTo(Math.round(p0.x)+0.5, Math.round(p0.y)+0.5)
    //ctx.quadraticCurveTo(this.points[0].x, this.points[0].y, this.points[1].x, this.points[1].y);     
    ctx.lineTo(Math.round(p1.x)+0.5, Math.round(p1.y)+0.5);        
    ctx.beginPath();    

    ctx.stroke();
    ctx.closePath()
}

Line.prototype.translate = function(x_step, y_step) {
    return new Line(this.points[0].add(new Point(x_step,y_step)), this.points[1].add(new Point(x_step,y_step)))
}
    
Line.prototype.rotate = function(around, angle) {
    return new Line(this.points[0].rotate(around, angle), this.points[1].rotate(around, angle))
}

Line.prototype.copy = function() {
    return new Line(this.points[0], this.points[1])
}
Line.prototype.reflect = function(line) { 
    var reflected = new Line(this.points[0].reflect(line), this.points[1].reflect(line))
    return reflected
}

Line.prototype.reverse = function() { 
    return new Line(this.points[1], this.points[0])
}

Line.prototype.skew = function(angle) {
    return new Line(this.points[0].skew(angle), this.points[1].skew(angle))
}

Line.prototype.y_skew = function(angle) {
    return new Line(this.points[0].y_skew(angle), this.points[1].y_skew(angle))
}

Line.prototype.invert = function() { 
    return new Line(new Point(this.points[0].x, this.points[1].y), new Point(this.points[1].x, this.points[0].y))
}

Line.prototype.scale = function(factor) {
    var line = new Line(this.points[0].scale(factor), this.points[1].scale(factor))
    return line.translate(this.points[0].x-line.points[0].x, this.points[0].y-line.points[0].y)
}

Line.prototype.evaluate = function(x) {
    return (this.slope() * x) + this.intercept()
};

Line.prototype.orthogonal = function(point) {
    var dest;
    if(this.slope() == 0) {
        dest  = new Point(point.x, (2 * this.points[0].y - point.y))
    } else if(typeof(this.slope()) == "undefined") {
        dest = new Point((2 * this.points[0].x - this.x), this.y)
    } else {
        var slope_ = -1/this.slope();
        var intercept_ = point.y - (slope_ * point.x);
        var x_ = (intercept_ - this.intercept()) / (this.slope() - slope_);
        var y_ = (slope_ * x_) + intercept_
        dest = new Point(x_, y_)
    }
    return new Line(point, dest)
};

/* 
*  
*  
*/
var Fractal = function() { 
    
}

Fractal.prototype.draw_arm = function(angle, point, length, ctx) {
    angle = -angle;
    this.points.push(point)
    point = point.translate(cos(angle) * length, sin(angle) * length)
    this.points.push(point)
}

// this is the guts of the recursive draw
Fractal.prototype.generateTurn = function(turn, p, internal_angle, length, depth, skew_angle, ctx, axis_angle, max_depth, draw_arms) {
    var angle = axis_angle;
    if(turn == 0) {
        angle -= skew_angle / 2
    } else if(turn == 1) {
        angle += internal_angle - skew_angle/2;
    } else if (turn ==2) {
        angle += skew_angle/2
        if(draw_arms) {
            if(depth == 0) {
                draw_arm(Math.PI/2+(internal_angle/2+axis_angle), p, length, ctx)
            } else {
                arm = new Fractal()
                arm.generate(p, internal_angle, length, depth-1, skew_angle, ctx, Math.PI/2+(internal_angle/2+axis_angle), max_depth)
                this.points = this.points.concat(arm.points);
            }
        }
    } else if(turn ==3) {
        angle += 3/2*skew_angle 
        angle += (2*Math.PI -internal_angle);
        if(draw_arms) {
            if(depth==0) {
                draw_arm(Math.PI/2-(internal_angle/2-axis_angle-2*skew_angle), p, length, ctx)
            } else {
                arm = new Fractal()
                arm.generate(p, internal_angle, length, depth-1, skew_angle, ctx, Math.PI/2-(internal_angle/2-axis_angle-2*skew_angle), max_depth)
                this.points = this.points.concat(arm.points);
            }
        }
    } else if(turn == 4) {
        angle -= skew_angle/2
    }
    
    if(depth == 0) {
        this.points.push(p)
        angle = -angle // becauses the y-axis is inverted
        p = p.translate(cos(angle) * length, sin(angle) * length)   
        this.points.push(p)                   
    } else {
        var f = new Fractal()
        p = f.generate(p, internal_angle, length, depth-1, skew_angle, ctx, angle, max_depth)
        this.points = this.points.concat(f.points);
    }
    return p;
    
}

Fractal.prototype.generate = function(origin, internal_angle, length, depth, skew_angle, ctx, axis_angle, max_depth, draw_arms) {
    if(isNaN(origin.x) || isNaN(internal_angle)) {
        throw "NaN"
    }
    this.points = [];
    var thisObject = this;
    var p = origin;       
    var turns = 5;
    for(var turn=0;turn<turns;turn++) {
        thisObject.generateTurn(turn, p, internal_angle, length, depth, skew_angle, ctx, axis_angle, max_depth, draw_arms)
        p = thisObject.points[thisObject.points.length - 1]
    }  
    
    return p
}

Fractal.prototype.draw = function(offset, jump, ctx) {
    if(offset + jump > this.points.length) {
        throw "Invalid num points"
    }
    for(var i=offset+1;i<offset+jump;i++) {
        var p = this.points[i]
        ctx.beginPath();    
        ctx.moveTo(Math.round(this.points[i-1].x)+0.5, Math.round(this.points[i-1].y)+0.5);
        ctx.lineTo(Math.round(p.x)+0.5, Math.round(p.y)+0.5)
        ctx.stroke();
        ctx.closePath()  
    }
}
    
Fractal.prototype.center = function(origin) {
    var translated = this.translate(origin.x - this.middle().x, origin.y - this.middle().y)
    var angle = Math.arccos(this.width() / Math.sqrt(Math.pow(this.width(), 2) +  Math.pow(this.height(), 2)))
    return translated.rotate(translated.middle(), -angle)
}

// requestAnim shim layer by Paul Irish
window.requestAnimFrame = (function () {
    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function ( /* function */ callback, /* DOMElement */ element) {
        window.setTimeout(callback, 1000 / 60);
    };
})();

// OPTIONS
var default_options = {
	//- FRACTAL INITILIZATION OPTIONS
	max_depth : 3,
	base_length : 5,
	zoom_increment : 1.25,
	initial_axis_angle : 0,
	origin : new Point(560,360),
	max_width : 500,
	zoom_level : 5,
	min_zoom_level : 1,
	max_zoom_level : 50,
	internal_angle : Math.PI/3,
	skew : 1,
	skew_angle : 0,
	
	//- FRACTAL ANIMATION OPTIONS
	animation_points : 10, // number of additional points to draw at each frame
	animation_delay : 250, // number of milliseconds to wait between each frame
	
	//- CANVAS OPTIONS
	canvas_id : 'canvas',
	ctx : null,
	line_width : 0.5,
	line_color : "#FFFFFF",
	line_shadow_color : "#47760b",
	canvas_width : 1900,
	canvas_height : 1200,
	line_blur :1,
}

var render = function(options) { 
    
    options.ctx.clearRect(0, 0, options.canvas_width, options.canvas_height);

    var fractal = new Fractal()
    fractal.generate(options.origin, options.internal_angle, Math.pow(options.zoom_increment, options.zoom_level) * options.base_length, options.max_depth, options.skew_angle, options.ctx,options.initial_axis_angle - options.skew_angle, options.max_depth)
    
    var start = null;
    var last = null
    var counter = 0;
    function step(timestamp) {
        if (!start) start = timestamp;
        var progress = Math.round((timestamp - start) / options.animation_delay)
        if(progress > last) {
            last = progress
            fractal.draw(counter * options.animation_points, options.animation_points,options.ctx)
            console.log("drawing")
            counter += 1
        }
        
        var next_start_idx = counter * options.animation_points
        var next_end_idx = next_start_idx + options.animation_points 
                
        // if we can draw a full batch
        if(next_start_idx < fractal.points.length && next_end_idx < fractal.points.length) {
            window.requestAnimationFrame(step);
        // if the last draw has fewer points than a full point
        } else if(next_start_idx < fractal.points.length) {
            fractal.draw(next_start_idx, fractal.points.length - next_start_idx,options.ctx)
        // if we are on the first frame and we want to draw all points at once
        } else if(counter == 0) {
            fractal.draw(1, fractal.points.length-1,options.ctx)
        }
    }

    window.requestAnimationFrame(step);
    
}        

var FractalRenderer = function (options) {
    if(typeof(options) !== "undefined") {
        for(var k in default_options) {
            if(typeof(options[k]) === "undefined") {
                options[k] = default_options[k];
            }
        }
    } else {
        options = default_options;
    }

    var canvas = document.getElementById(options.canvas_id);
        
    if (canvas.getContext) {
        canvas.width = options.canvas_width;
        canvas.height = options.canvas_height; 
        options.ctx = canvas.getContext('2d');
        options.ctx.lineWidth = options.line_width
        options.ctx.strokeStyle = options.line_color;
        options.ctx.shadowBlur=options.line_blur;
        options.ctx.shadowColor=options.line_shadow_color;
        canvas.addEventListener("mousewheel", zoom, false);
        canvas.addEventListener("DOMMouseScroll", zoom, false);
        render(options);
    };
    
    if(options.customizable) {
        var internal_angle_input = document.getElementById("internal_angle")
        var skew_angle_input = document.getElementById("skew_angle")
        
        var increase_depth = function(e) {
            e.preventDefault();
            options.max_depth += 1;
                        console.log("incrfeasing")
            render(options);
        }
        var decrease_depth = function(e) {
            e.preventDefault();
            options.max_depth = Math.max(options.max_depth-1, 0);
            render(options);
        }

        var internal_angle_plus = function(e) {
            internal_angle_input.value = parseFloat(internal_angle_input.value) + 5
            set_internal_angle(e);
        }

        var decrement_internal_angle = function(e) {
            internal_angle_input.value = parseFloat(internal_angle_input.value) - 5
            set_internal_angle(e);
        }

        var increment_skew_angle = function(e) {
            skew_angle_input.value = parseFloat(skew_angle_input.value) + 5
            set_skew_angle(e);
        }

        var decrement_skew_angle = function(e) {
            skew_angle_input.value = parseFloat(skew_angle_input.value) - 5
            set_skew_angle(e);
        }

        var set_internal_angle = function(e) {
            e.preventDefault(e);
            options.internal_angle = (parseFloat(document.getElementById("internal_angle").value) / 360) * 2*Math.PI
            render()
        }

        var set_skew_angle = function(e) {
            e.preventDefault(e);
            options.skew_angle = (parseFloat(document.getElementById("skew_angle").value) / 360) * 2*Math.PI
            render(options)
        }
        
        var zoom = function(e) {
            e.preventDefault()
            if(e.wheelDelta > 0) {
                options.zoom_level = Math.max(options.min_zoom_level, options.zoom_level - 1);
            } else {
                options.zoom_level = Math.min(options.max_zoom_level, options.zoom_level + 1);
            }   
            render();
        }
        
        document.getElementById("increase_depth").addEventListener("click", increase_depth, false);
        document.getElementById("decrease_depth").addEventListener("click", decrease_depth, false);
        document.getElementById("increment_internal_angle_button").addEventListener("click", internal_angle_plus, false);
        document.getElementById("increment_skew_angle_button").addEventListener("click", increment_skew_angle, false);
        document.getElementById("decrement_internal_angle_button").addEventListener("click", decrement_internal_angle, false);
        document.getElementById("decrement_skew_angle_button").addEventListener("click", decrement_skew_angle, false);
        internal_angle_input.value = (options.internal_angle / (2*Math.PI)) * 360
        skew_angle_input.value = (options.skew_angle / (2*Math.PI)) * 360
    }
    
}







